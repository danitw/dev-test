// Escreva uma função que converta a data de entrada do usuário formatada como MM/DD/YYYY em um formato exigido por uma API (YYYYMMDD). O parâmetro "userDate" e o valor de retorno são strings.

// Por exemplo, ele deve converter a data de entrada do usuário "31/12/2014" em "20141231" adequada para a API.
// import { ptBR } from "date-fns/locale";
// import { format } from "date-fns";

const { format } = require("date-fns");
const { ptBR } = require("date-fns/locale");

function formatDate(userDate) {
  // format from M/D/YYYY to YYYYMMDD
  return format(new Date(userDate), "yyyyMd", {
    locale: ptBR,
  });
}

console.log(formatDate("12/31/2014"));

<?php


/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*
Implemente uma função que ao receber um array associativo contendo arquivos e donos, retorne os arquivos agrupados por dono. 

Por exemplo, um array ["Input.txt" => "Jose", "Code.py" => "Joao", "Output.txt" => "Jose"] a função groupByOwners deveria retornar ["Jose" => ["Input.txt", "Output.txt"], "Joao" => ["Code.py"]].


*/

class FileOwners
{
  public static function groupByOwners($files)
  {
    $newArray = array();

    foreach ($files as $key => $value) {
      if ($newArray[$value]) {
        array_push($newArray[$value], $key);
      } else {
        $newArray[$value] = [$key];
      }
    }

    return $newArray;
  }
}

$files = array(
  "Input.txt" => "Jose",
  "Code.py" => "Joao",
  "Output.txt" => "Jose",
  "Click.js" => "Maria",
  "Out.php" => "Joao",
  "Reality.rs" => "Daniel",
  "Kernel.go" => "Daniel",
  "Love.lua" => "Daniel",
);

var_dump(FileOwners::groupByOwners($files));
